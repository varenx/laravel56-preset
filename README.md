# Laravel 5.6 Preset by Varenx

Installation and running webpack with watch.

```
composer require varenx/laravel56-preset
php artisan preset varenxlaravel
npm i && composer dump-auto
webpack --watch --watch-poll webpack.config.js
```
