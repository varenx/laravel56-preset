<?php
Route::get('/', 'FrontendController@index')->name('home');
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('home');
});
