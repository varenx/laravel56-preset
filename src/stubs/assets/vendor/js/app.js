import jQuery from 'jquery';
window.$ = jQuery;
import 'select2';
require('bootstrap');
window.Vue = require('vue');

// All select2 instances should use our bootstrap4 theme
$.fn.select2.defaults.set('theme', 'bootstrap4');
