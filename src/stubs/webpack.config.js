var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

var APP_DIR = path.resolve(__dirname, './');

module.exports = {
    entry: {
        'js/frontend': APP_DIR + '/resources/assets/frontend/js/app.js',
        'js/admin': APP_DIR + '/resources/assets/admin/js/app.js',
        'js/vendor': APP_DIR + '/resources/assets/vendor/js/app.js',
        'css/frontend': APP_DIR + '/resources/assets/frontend/sass/app.scss',
        'css/admin': APP_DIR + '/resources/assets/admin/sass/app.scss',
        'css/vendor': APP_DIR + '/resources/assets/vendor/sass/app.scss'
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, './public')
    },
    module: {
        rules: [
            {
                test: /\.s[ca]ss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader',
                        'sass-loader'
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.css$/,
                use: [
                    'css-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'

            },
            {
              test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=fonts/[name].[hash].[ext]&publicPath=../'
            }, {
              test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=fonts/[name].[hash].[ext]&publicPath=../'
            }, {
              test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'url-loader?limit=10000&mimetype=application/octet-stream&name=fonts/[name].[hash].[ext]&publicPath=../'
            }, {
              test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'file-loader?name=fonts/[name].[hash].[ext]&publicPath=../'
            }, {
              test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'url-loader?limit=10000&mimetype=image/svg+xml&name=fonts/[name].[hash].[ext]&publicPath=../'
            }
        ]
    },

    plugins: [
        new CleanWebpackPlugin([ 'public/css', 'public/js' ], {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new ExtractTextPlugin('[name].[chunkhash].css'),
        function () {
            this.plugin('done', stats => {
                var json = stats.toJson().assetsByChunkName,
                    admin_css_key = 'css/admin',
                    frontend_css_key = 'css/frontend',
                    vendor_css_key = 'css/vendor';
                try {
                    fs.unlinkSync(path.join('./public', json[admin_css_key][0]));
                    fs.unlinkSync(path.join('./public', json[frontend_css_key][0]));
                    fs.unlinkSync(path.join('./public', json[vendor_css_key][0]));
                } catch (e) {}
                json[admin_css_key] = json[admin_css_key][1];
                json[frontend_css_key] = json[frontend_css_key][1];
                json[vendor_css_key] = json[vendor_css_key][1];
                fs.writeFileSync(
                    path.join(__dirname, 'public/manifest.json'),
                    JSON.stringify(json)
                );
            });
        }
    ]
};
