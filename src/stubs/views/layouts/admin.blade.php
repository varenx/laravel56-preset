<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin</title>
        <link rel="stylesheet" href="{{ manifest('css/vendor') }}" type="text/css" />
        <link rel="stylesheet" href="{{ manifest('css/admin') }}" type="text/css" />

    </head>
    <body>
        @yield('content')
        <script src="{{ manifest('js/vendor') }}"></script>
        <script src="{{ manifest('js/admin') }}"></script>
    </body>
</html>

