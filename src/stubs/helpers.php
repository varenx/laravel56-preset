<?php
if (!function_exists('manifest')) {
    function manifest($key)
    {
        $manifest = json_decode(
            file_get_contents(public_path('manifest.json'))
        );
        return $manifest->$key;
    }
}
