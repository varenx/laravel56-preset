<?php

namespace Varenx\LaravelPreset;

use Illuminate\Foundation\Console\PresetCommand;
use Illuminate\Support\ServiceProvider;
use Varenx\LaravelPreset\Preset;

class VarenxServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        PresetCommand::macro('varenxpreset', function ($command) {
            $command->info('Bootstrapping with Varenx\'s Laravel preset');
            $command->info('This preset installs: ');
            $command->info('jquery 3.3.1');
            $command->info('bootstrap 4.1.3');
            $command->info('font-awesome 4.7.0');
            $command->info('vue 2.5.16');
            $command->info('It also sets up a default resource structure and webpack');
            $command->info('NOTE: After the preset is run, make sure you run');
            $command->info('composer dump-auto && npm i');
            Preset::install();
        });
    }
}
