<?php
namespace Varenx\LaravelPreset;

use Illuminate\Foundation\Console\Presets\Preset as LaravelPreset;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class Preset extends LaravelPreset
{

    public static function install()
    {
        static::cleanAssets();
        static::cleanBuildFiles();
        static::updatePackages();
        static::setupBuildFiles();
        static::setupAssetStubs();
        static::setupViewStubs();
        static::updateComposer();
    }

    protected static function cleanAssets()
    {
        File::cleanDirectory(resource_path('assets'));
        File::cleanDirectory(resource_path('js'));
    }

    protected static function cleanBuildFiles()
    {
        File::delete(base_path('webpack.mix.js'));
    }

    protected static function setupAssetStubs()
    {
        File::copyDirectory(__DIR__ . '/stubs/assets', resource_path('assets'));
    }

    protected static function setupViewStubs()
    {
        File::delete(resource_path('views/welcome.blade.php'));
        File::copyDirectory(__DIR__ . '/stubs/views/layouts', resource_path('views/layouts'));
        File::copyDirectory(__DIR__ . '/stubs/views/frontend', resource_path('views/frontend'));
        File::copyDirectory(__DIR__ . '/stubs/views/admin', resource_path('views/admin'));
        // Overwrite our routes
        File::copy(__DIR__ . '/stubs/web.php', base_path('/routes/web.php'));
        // Controllers
        File::copy(__DIR__ . '/stubs/controllers/FrontendController.php', app_path('Http/Controllers/FrontendController.php'));
        File::copy(__DIR__ . '/stubs/controllers/AdminController.php', app_path('Http/Controllers/AdminController.php'));
        // Helpers
        File::copy(__DIR__ . '/stubs/helpers.php', app_path('Http/helpers.php'));
    }

    protected static function setupBuildFiles()
    {
        // Our webpack config file
        File::copy(
            __DIR__ . '/stubs/webpack.config.js',
            base_path('webpack.config.js')
        );
    }

    protected static function updatePackageArray($packages)
    {
        return array_merge(
            [
                'autoprefixer' => '^8.5.0',
                'babel-core' => '^6.26.3',
                'babel-loader' => '^7.1.4',
                'bootstrap' => '^4.1.3',
                'clean-webpack-plugin' => '^0.1.19',
                'css-loader' => '^0.28.11',
                'extract-text-webpack-plugin' => '^4.0.0-beta.0',
                'file-loader' => '^1.1.11',
                'font-awesome' => '^4.7.0',
                'jquery' => '^3.3.1',
                'node-sass' => '^4.9.0',
                'popper.js' => '^1.14.3',
                'postcss-loader' => '^2.1.5',
                'sass-loader' => '^7.0.1',
                'select2' => '^4.0.6-rc.1',
                'select2-bootstrap4-theme' => '^1.0.0',
                'style-loader' => '^0.21.0',
                'url-loader' => '^1.0.1',
                'vue' => '^2.5.16',
                'webpack' => '^4.8.3',
                'webpack-cli' => '^3.1.2',
            ],
            Arr::except(
                $packages, [
                    'axios',
                    'bootstrap',
                    'popper.js',
                    'jquery',
                    'laravel-mix',
                    'lodash',
                    'vue'
                ]
            ) // Arr::except
        ); // array_merge
    }

    protected static function updateComposer()
    {
        $composer = json_decode(file_get_contents(base_path('composer.json')));
        $composer->autoload->files = [
            'app/Http/helpers.php'
        ];
        // Save composer
        file_put_contents(base_path('composer.json'), json_encode($composer, JSON_PRETTY_PRINT));
    }
}
